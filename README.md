# README #

### What is this repository for? ###

* Hacker News site for writing posts and comments after user registration
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* AngularJS v1.3.10, Node.js v0.10.31 and MongoDB 2.6.7
* To start the application run the www.js file

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact